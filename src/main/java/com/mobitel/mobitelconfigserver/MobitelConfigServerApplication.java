package com.mobitel.mobitelconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class MobitelConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobitelConfigServerApplication.class, args);
	}

}
